const path = require('path')
const fs = require('fs')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin

// need .env file (env.sample as example)
if (!fs.existsSync('./.env')) {
  throw new Error('need .env file')
}

const Dotenv = require('dotenv-webpack')
const pluginsForClientEntry = [new Dotenv()]

if (process.env.ANALYZER_MODE === 'on') {
  pluginsForClientEntry.push(new BundleAnalyzerPlugin())
}

module.exports = {
  outputDir: path.resolve(__dirname, './dist'),
  assetsDir: 'assets',
  productionSourceMap: false,
  lintOnSave: process.env.NODE_ENV !== 'production',
  runtimeCompiler: true,
  configureWebpack: {
    entry: {
      app: './src/entry-client',
    },
    target: 'web',
    node: false,
    plugins: pluginsForClientEntry,
    output: {
      filename: 'assets/js/[name].[hash].js',
      chunkFilename: 'assets/js/[name].[hash].js',
      globalObject: 'this',
    },
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 250000,
        cacheGroups: {
          vendors: {
            name: 'vnd_',
            test: /[\\/]node_modules[\\/]/,
            priority: -10,
            chunks: 'all',
          },
        },
      },
    },
  },
  css: {
    extract: true,
    sourceMap: false,
  },
  chainWebpack: (config) => {
    // config.plugins.delete('preload')
    // config.plugins.delete('prefetch')
    config.plugin('html').tap((args) => {
      args[0].template = 'shared/templates/client.template.html'
      args[0].filename = 'index.html'
      return args
    })
  },

  devServer: {
    host: process.env.PUB_HOST,
    stats: 'errors-only',
    https:
      process.env.SSL_MODE === 'on'
        ? {
            key: fs.readFileSync(
              path.resolve(__dirname, './shared/ssl/localhost.pem')
            ),
            cert: fs.readFileSync(
              path.resolve(__dirname, './shared/ssl/localhost.crt')
            ),
          }
        : undefined,
    hot: true,
    contentBase: path.resolve(__dirname, './dist'),
    watchContentBase: true,
    // compress: true,
    port: process.env.PUB_PORT,
  },
}
