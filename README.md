# demo-todo

http://todo.rlib.ru/

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```

### Run bundle analyzer

```
yarn analyzer
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
