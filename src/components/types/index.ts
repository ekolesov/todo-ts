import { TaskModel } from '@/store/types'

export interface BaseModalComponent extends Vue {
  show(): void
  hide(): void
}

export interface TaskModalComponent extends Vue {
  showTask({ task, cb }: { task: TaskModel; cb: Function }): void
}

export interface TextInputComponent extends Vue {
  setFocus(): void
}
