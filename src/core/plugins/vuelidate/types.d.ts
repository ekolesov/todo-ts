import Vuelidate from '@types/vuelidate'
import Vue from 'vue'

declare module 'vue/types/vue' {
  interface Vue {
    $v: Vuelidate
  }
}
