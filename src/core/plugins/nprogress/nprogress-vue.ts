import Vue from 'vue'
import NProgress from 'nprogress'
import './nprogress.styl'

NProgress.configure({
  showSpinner: false,
  easing: 'ease',
  speed: 500,
})

export default {
  install: function (vue: typeof Vue): void {
    Object.defineProperties(vue.prototype, {
      $np: {
        get: function () {
          return NProgress
        },
      },
    })
  },
}
