import { NProgress } from '@types/nprogress'
import Vue from 'vue'

declare module 'vue/types/vue' {
  interface Vue {
    $np: NProgress
  }
}
