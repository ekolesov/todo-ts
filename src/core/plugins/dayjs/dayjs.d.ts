import { dayjs } from 'dayjs'
import Vue from 'vue'

declare module 'vue/types/vue' {
  interface Vue {
    $dayjs: dayjs.Dayjs
  }

  interface VueConstructor {
    dayjs: dayjs.Dayjs
  }
}
