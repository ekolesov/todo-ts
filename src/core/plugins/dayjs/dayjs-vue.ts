import Vue from 'vue'
import dayjs from 'dayjs'
import 'dayjs/locale/ru'
import localizedFormat from 'dayjs/plugin/localizedFormat'

dayjs.locale('ru')
dayjs.extend(localizedFormat)

export default {
  install: function (vue: typeof Vue): void {
    Object.defineProperties(vue.prototype, {
      $dayjs: {
        get: function () {
          return dayjs
        },
      },
    })

    vue.dayjs = dayjs
  },
}
