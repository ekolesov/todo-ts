import { MetaInfo } from 'vue-meta/types/vue-meta'
import { MetaViewData, MetaRenderData } from './types'

const host = process.env.META_URL
const defaultImage = host + '/assets/img/cover.jpg'
const defaultTitle = 'Vue-app'

class MetaHelper {
  private _clearMeta = (value: string) => {
    return value.replace(/&(amp;)?shy;/g, '')
  }

  public metaData = (meta: MetaViewData) => {
    const metaTitle = meta.title ? this._clearMeta(meta.title) : defaultTitle

    const isShort = meta.isShort === true

    const metaRender: MetaRenderData = {
      title: metaTitle,
      htmlAttrs: {
        lang: 'ru',
      },
      meta: isShort
        ? []
        : [
            {
              vmid: 'og:type',
              property: 'og:type',
              content: 'website',
            },
            {
              vmid: 'og:title',
              property: 'og:title',
              content: metaTitle,
            },
            {
              vmid: 'mrc__share_title',
              name: 'mrc__share_title',
              content: metaTitle,
            },
          ],
    }

    if (meta.description) {
      const metaDescription = this._clearMeta(meta.description)

      metaRender.meta.push({
        vmid: 'description',
        property: 'description',
        content: metaDescription,
      })

      if (!isShort) {
        metaRender.meta.push({
          vmid: 'mrc__share_description',
          name: 'mrc__share_description',
          content: metaDescription,
        })
        metaRender.meta.push({
          vmid: 'og:description',
          property: 'og:description',
          content: metaDescription,
        })
      }
    }

    if (meta.url) {
      metaRender.link = metaRender.link || []
      metaRender.link = [
        {
          vmid: 'canonical',
          rel: 'canonical',
          href: host + meta.url,
        },
      ]

      if (!isShort) {
        metaRender.meta.push({
          vmid: 'og:url',
          property: 'og:url',
          content: host + meta.url,
        })
      }
    }

    if (!isShort) {
      if (!meta.img) {
        meta.img = defaultImage
      }

      metaRender.link = metaRender.link || []
      metaRender.link.push({
        vmid: 'image_src',
        rel: 'image_src',
        href: meta.img,
      })

      metaRender.meta.push({
        vmid: 'og:image',
        property: 'og:image',
        content: meta.img,
      })
    }

    return metaRender as MetaInfo
  }
}

export default MetaHelper
