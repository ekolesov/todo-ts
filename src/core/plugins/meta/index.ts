import Vue from 'vue'
import VueMeta from 'vue-meta'
import MetaHelper from './helper'

Vue.prototype.$metaHelper = new MetaHelper()

Vue.use(VueMeta)
