import Vue from 'vue'
import MetaHelper from './helper'

export interface MetaViewData {
  title?: string
  isShort: boolean
  description?: string
  url?: string
  img?: string
}

export interface MetaTagData {
  vmid?: string
  property?: string
  content?: string
  name?: string
  rel?: string
  href?: string
}

export interface MetaRenderData {
  title: string
  htmlAttrs: {
    lang: string
  }
  meta: Array<MetaTagData>
  link?: Array<MetaTagData>
}

export interface MetaHelperType {
  metaData(meta: MetaViewData): MetaRenderData
}

declare module 'vue/types/vue' {
  interface Vue {
    $metaHelper: MetaHelper
  }
}
