import Vue from 'vue'

const requireComponent = require.context('@/components/_base', true)

requireComponent.keys().forEach((fileName) => {
  const componentConfig = requireComponent(fileName)
  const componentPath = fileName.split('/').pop() as string

  if (componentPath) {
    const componentName = componentPath.replace(/\.\w+$/, '')
    Vue.component(componentName, componentConfig.default || componentConfig)
  }
})
