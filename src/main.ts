import Vue from 'vue'
import App from './App.vue'

import { createRouter } from './router'
import { createStore } from './store'

// Styles
import '@/stylus/_core/resources.css'
import '@/stylus/main.styl'

// Plugins
import '@/core/plugins/meta'
import '@/core/plugins/vuelidate'
import '@/core/plugins/dayjs'

// Global register base components
import '@/core/ui'

export function createApp() {
  const router = createRouter()
  const store = createStore()

  const app = new Vue({
    data: {
      isMobileOnly: false,
      timeStamp: 0,
    },
    router,
    store,
    template: '<App/>',
    components: {
      App,
    },
  })

  return { app, router, store }
}
