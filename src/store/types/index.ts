export interface UserModel {
  login: string
  password: string
}

export interface TaskModel {
  id?: string
  title: string
  isComplete: boolean
  end: Date | number | null
  closed: Date | number | null
  cb?: Function
}
