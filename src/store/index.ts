import Vue from 'vue'
import Vuex from 'vuex'

import AuthData from '@/store/modules/auth'
import TasksData from '@/store/modules/tasks'

Vue.use(Vuex)
const storage = process.env.STORAGE

export function createStore() {
  const store = new Vuex.Store({
    modules: {
      AuthData,
      TasksData,
    },

    getters: {
      isAuth: (state) => !!state.AuthData.user,
    },

    strict: true,
  })

  store.subscribe((mutation, state) => {
    if (window) {
      if (mutation.type === 'AuthData/SET_USER') {
        const user = state.AuthData.user
        if (user) {
          const userData = { ...user }
          userData.password = '*** fakePass ***'
          window[storage].setItem('userData', JSON.stringify(userData))

          const storedTasks = window[storage].getItem(`tasks_${user.login}`)
          if (storedTasks) {
            store.dispatch('TasksData/setList', JSON.parse(storedTasks))
          }
        } else {
          window[storage].removeItem('userData')
          store.dispatch('TasksData/clearList')
        }
      }

      if (
        ['TasksData/SET_TASK', 'TasksData/REMOVE_TASK'].includes(mutation.type)
      ) {
        const data = state.TasksData.list
        const user = state.AuthData.user
        if (user && data) {
          if (data.length) {
            window[storage].setItem(`tasks_${user.login}`, JSON.stringify(data))
          } else {
            window[storage].removeItem(`tasks_${user.login}`)
          }
        }
      }
    }
  })

  return store
}
