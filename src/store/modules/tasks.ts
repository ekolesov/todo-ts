import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import { v4 as uuidv4 } from 'uuid'
import { TaskModel } from '../types'

@Module({ namespaced: true, stateFactory: true })
class TasksData extends VuexModule {
  public list: TaskModel[] = []

  @Mutation
  public SET_TASK(task: TaskModel): void {
    const indexEl = this.list.findIndex((item) => item.id === task.id)
    if (indexEl >= 0) {
      this.list.splice(indexEl, 1, task)
    } else {
      task.id = uuidv4()
      this.list.push(task)
    }
  }

  @Mutation
  public REMOVE_TASK(task: TaskModel): void {
    this.list = this.list.filter((item) => item.id != task.id)
  }

  @Mutation
  public SET_LIST(list: TaskModel[]): void {
    this.list = list
  }

  @Action
  public async saveTask(task: TaskModel) {
    this.context.commit('SET_TASK', task)
  }

  @Action
  public async removeTask(task: TaskModel) {
    this.context.commit('REMOVE_TASK', task)
  }

  @Action
  public async setList(list: TaskModel[]) {
    this.context.commit('SET_LIST', list)
  }

  @Action
  public async clearList() {
    this.context.commit('SET_LIST', [])
  }
}

export default TasksData
