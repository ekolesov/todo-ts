import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import { UserModel } from '../types'

@Module({ namespaced: true, stateFactory: true })
class UserData extends VuexModule {
  public user: UserModel | null = null

  @Mutation
  public SET_USER(state: UserModel): void {
    this.user = state
  }

  @Action
  public async login(user: UserModel) {
    this.context.commit('SET_USER', user)
  }

  @Action
  public async logout() {
    this.context.commit('SET_USER', null)
  }
}
export default UserData
