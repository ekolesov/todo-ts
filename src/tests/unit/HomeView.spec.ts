import { createLocalVue, mount } from '@vue/test-utils'
import Vuex, { Store } from 'vuex'

import Home from '@/views/Home.vue'
import AuthData from '@/store/modules/auth'
import TasksData from '@/store/modules/tasks'

import { TaskModel, UserModel } from '@/store/types'

import Button from '@/components/_base/ui/Button.vue'
import BaseModal from '@/components/_base/modals/BaseModal.vue'
import Checkbox from '@/components/_base/ui/Checkbox.vue'
import TextInput from '@/components/_base/ui/TextInput.vue'

import VueDayJs from '@/core/plugins/dayjs/dayjs-vue'
import Vuelidate from 'vuelidate'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueDayJs)
localVue.use(Vuelidate)
localVue.component('Button', Button)
localVue.component('BaseModal', BaseModal)
localVue.component('Checkbox', Checkbox)
localVue.component('TextInput', TextInput)

describe('views/Login.vue', () => {
  // const mockRouter = {
  //   push: jest.fn(),
  // }
  const user: UserModel = { login: 'user', password: 'pass' }
  // eslint-disable-next-line
  let store: Store<any>
  let taskData: TaskModel

  beforeEach(() => {
    taskData = {
      id: 'idTask',
      title: 'TaskTitle',
      isComplete: false,
      end: null,
      closed: null,
    }

    store = new Store({
      modules: {
        AuthData,
        TasksData,
      },
      getters: {
        isAuth: (state) => !!state.AuthData.user,
      },
      strict: true,
    })
  })

  it('should render empty view', async () => {
    const wrapper = mount(Home, {
      localVue,
      store,
      stubs: ['TaskModal', 'TaskItem'],
    })
    await store.dispatch('AuthData/login', user)

    expect(wrapper.element).toMatchSnapshot('homeEmptyView')
  })

  it('should render list view', async () => {
    const wrapper = mount(Home, {
      localVue,
      store,
    })
    await store.dispatch('AuthData/login', user)
    await store.dispatch('TasksData/saveTask', taskData)

    expect(wrapper.element).toMatchSnapshot('homeListView')
  })
})
