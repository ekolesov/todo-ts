import { shallowMount } from '@vue/test-utils'
import Button from '@/components/_base/ui/Button.vue'

describe('ui/Button.vue', () => {
  it('should render label slot', () => {
    const toBeValue = 'ButtonLabel'
    const wrapper = shallowMount(Button, {
      slots: {
        label: toBeValue,
      },
    })

    expect(wrapper.text()).toBe(toBeValue)
  })

  it('should render custom style class', () => {
    const toBeValue = 'customStyleClass'
    const wrapper = shallowMount(Button, {
      propsData: {
        styleClass: toBeValue,
      },
    })

    expect(wrapper.find('.' + toBeValue).exists()).toBeTruthy()
  })

  it('should render style class for "primary" attribute', () => {
    const toBeValue = 'button--primary'
    const wrapper = shallowMount(Button, {
      attrs: {
        primary: '',
      },
    })

    expect(wrapper.find('.' + toBeValue).exists()).toBeTruthy()
  })

  it('should render style class for "info" attribute', () => {
    const toBeValue = 'button--info'
    const wrapper = shallowMount(Button, {
      attrs: {
        info: '',
      },
    })

    expect(wrapper.find('.' + toBeValue).exists()).toBeTruthy()
  })

  it('should render style class and stop emit "click" for "isDisabled" prop', async () => {
    const toBeValue = 'button--disabled'
    const wrapper = shallowMount(Button, {
      propsData: {
        isDisabled: true,
      },
    })
    await wrapper.trigger('click')

    expect(wrapper.find('.' + toBeValue).exists()).toBeTruthy()
    expect(wrapper.emitted().change).toBeUndefined()
  })

  it('should emit click event', async () => {
    const wrapper = shallowMount(Button)
    await wrapper.trigger('click')

    expect(wrapper.emitted().click).toBeDefined()
  })
})
