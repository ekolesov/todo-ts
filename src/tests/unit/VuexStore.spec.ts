import { TaskModel, UserModel } from '@/store/types'
import AuthData from '@/store/modules/auth'
import TasksData from '@/store/modules/tasks'
import Vuex, { Store } from 'vuex'
import { createLocalVue } from '@vue/test-utils'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('store/modules/tasks.ts', () => {
  let taskData: TaskModel
  // eslint-disable-next-line
  let store: Store<any>

  beforeEach(() => {
    taskData = {
      id: 'idTask',
      title: 'TaskTitle',
      isComplete: false,
      end: null,
      closed: null,
    }
    store = new Store({
      modules: {
        TasksData,
      },
      strict: true,
    })
  })

  it('should set task', () => {
    const tasksModule = new TasksData(TasksData)
    tasksModule.SET_TASK(taskData)

    expect(tasksModule.list.length).toBe(1)
  })

  it('should set task without double', () => {
    const tasksModule = new TasksData(TasksData)
    tasksModule.SET_TASK(taskData)
    tasksModule.SET_TASK(taskData)
    tasksModule.SET_TASK(taskData)

    expect(tasksModule.list.length).toBe(1)
  })

  it('should remove task', async () => {
    const tasksModule = new TasksData(TasksData)
    tasksModule.SET_TASK(taskData)

    expect(tasksModule.list.length).toBe(1)

    tasksModule.REMOVE_TASK(taskData)

    expect(tasksModule.list.length).toBe(0)
  })

  it('should set list', async () => {
    const tasksModule = new TasksData(TasksData)
    tasksModule.SET_LIST([taskData])

    expect(tasksModule.list.length).toBe(1)
  })

  it('should save task from action', async () => {
    await store.dispatch('TasksData/saveTask', taskData)

    expect(store.state.TasksData.list).toEqual([taskData])
  })

  it('should remove task from action', async () => {
    await store.dispatch('TasksData/saveTask', taskData)
    await store.dispatch('TasksData/removeTask', taskData)

    expect(store.state.TasksData.list).toEqual([])
  })

  it('should set list from action', async () => {
    await store.dispatch('TasksData/setList', [taskData])

    expect(store.state.TasksData.list).toEqual([taskData])
  })

  it('should clear list', async () => {
    await store.dispatch('TasksData/clearList')

    expect(store.state.TasksData.list).toEqual([])
  })
})

describe('store/modules/auth.ts', () => {
  const user: UserModel = { login: 'user', password: 'pass' }
  // eslint-disable-next-line
  let store: Store<any>

  beforeEach(() => {
    store = new Store({
      modules: {
        AuthData,
      },
      getters: {
        isAuth: (state) => !!state.AuthData.user,
      },
      strict: true,
    })
  })

  it('should set user', () => {
    const authModule = new AuthData(AuthData)
    authModule.SET_USER(user)

    expect(authModule.user).toEqual(user)
  })

  it('should set user after login', async () => {
    await store.dispatch('AuthData/login', user)

    expect(store.getters.isAuth).toBeTruthy()
    expect(store.state.AuthData.user).toEqual(user)
  })

  it('should remove user after logout', async () => {
    await store.dispatch('AuthData/logout')

    expect(store.getters.isAuth).toBeFalsy()
    expect(store.state.AuthData.user).toEqual(null)
  })
})
