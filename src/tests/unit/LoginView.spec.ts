import { createLocalVue, mount } from '@vue/test-utils'
import Vuex, { Store } from 'vuex'

import Login from '@/views/Login.vue'
import AuthData from '@/store/modules/auth'

import Button from '@/components/_base/ui/Button.vue'
import TextInput from '@/components/_base/ui/TextInput.vue'

import Vuelidate from 'vuelidate'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Vuelidate)
localVue.component('Button', Button)
localVue.component('TextInput', TextInput)

describe('views/Login.vue', () => {
  // eslint-disable-next-line
  let store: Store<any>
  const mockRouter = {
    push: jest.fn(),
  }

  beforeEach(() => {
    store = new Store({
      modules: {
        AuthData,
      },
      getters: {
        isAuth: (state) => !!state.AuthData.user,
      },
      strict: true,
    })
  })

  it('should render view', () => {
    const wrapper = mount(Login, {
      localVue,
      store,
    })

    expect(wrapper.element).toMatchSnapshot('loginView')
  })

  it('should set auth user', async () => {
    const wrapper = mount(Login, {
      data: () => {
        return {
          loginValue: 'user',
          passwordValue: 'pass',
        }
      },
      localVue,
      store,
      mocks: {
        $router: mockRouter,
      },
    })
    const elemButton = wrapper.findComponent(Button)
    await elemButton.trigger('click')

    expect(store.getters.isAuth).toBeTruthy()
  })

  it('should validate inputs', async () => {
    const wrapper = mount(Login, {
      localVue,
      store,
      mocks: {
        $router: mockRouter,
      },
    })
    const elemButton = wrapper.findComponent(Button)
    await elemButton.trigger('click')

    expect(wrapper.vm.$v.loginValue.required).toBeFalsy()
    expect(wrapper.vm.$v.passwordValue.required).toBeFalsy()

    await wrapper.setData({
      loginValue: 'u',
      passwordValue: 'p',
    })
    await elemButton.trigger('click')

    expect(wrapper.vm.$v.loginValue.minLength).toBeFalsy()
    expect(wrapper.vm.$v.passwordValue.minLength).toBeFalsy()

    await wrapper.setData({
      loginValue: 'use',
      passwordValue: 'pas',
    })
    await elemButton.trigger('click')

    expect(wrapper.vm.$v.$invalid).toBeFalsy()
  })
})
