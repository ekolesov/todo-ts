import { createLocalVue, mount } from '@vue/test-utils'
import TaskModal from '@/components/modals/TaskModal.vue'

import Button from '@/components/_base/ui/Button.vue'
import BaseModal from '@/components/_base/modals/BaseModal.vue'
import Checkbox from '@/components/_base/ui/Checkbox.vue'
import TextInput from '@/components/_base/ui/TextInput.vue'

import { TaskModel } from '@/store/types'
import { TaskModalComponent } from '@/components/types'

import VueDayJs from '@/core/plugins/dayjs/dayjs-vue'
import Vuelidate from 'vuelidate'

const localVue = createLocalVue()
localVue.use(VueDayJs)
localVue.use(Vuelidate)
localVue.component('Button', Button)
localVue.component('BaseModal', BaseModal)
localVue.component('Checkbox', Checkbox)
localVue.component('TextInput', TextInput)

const defDate = localVue.dayjs('2020-12-01').startOf('d')

describe('modals/TaskModal.vue', () => {
  let taskData: TaskModel
  beforeEach(() => {
    taskData = {
      id: 'idTask',
      title: 'TaskTitle',
      isComplete: false,
      end: +defDate.add(-1, 'd'),
      closed: null,
    }
  })

  it('should render task editor', async () => {
    const wrapper = mount(TaskModal, {
      propsData: {
        task: taskData,
      },
      stubs: ['Datepicker'],
      localVue,
    })
    const instance = wrapper.findComponent(TaskModal).vm as TaskModalComponent
    instance.showTask({ task: taskData, cb: jest.fn() })
    await wrapper.vm.$nextTick()

    expect(wrapper.element).toMatchSnapshot('editTaskModal')
  })

  it('should render task info', async () => {
    taskData.closed = +defDate.add(-1, 'd')
    taskData.isComplete = true
    const wrapper = mount(TaskModal, {
      propsData: {
        task: taskData,
      },
      stubs: ['Datepicker'],
      localVue,
    })
    const instance = wrapper.findComponent(TaskModal).vm as TaskModalComponent
    instance.showTask({ task: taskData, cb: jest.fn() })
    await wrapper.vm.$nextTick()

    expect(wrapper.element).toMatchSnapshot('completedTaskModal')
  })

  it('should render header for new task', async () => {
    taskData.id = ''
    const wrapper = mount(TaskModal, {
      propsData: {
        task: taskData,
      },
      stubs: ['Datepicker'],
      localVue,
    })
    const instance = wrapper.findComponent(TaskModal).vm as TaskModalComponent
    instance.showTask({ task: taskData, cb: jest.fn() })
    await wrapper.vm.$nextTick()

    expect(wrapper.find('.modal__header').text()).toBe('Новая задача')
  })

  it('should render header for edit task', async () => {
    const wrapper = mount(TaskModal, {
      propsData: {
        task: taskData,
      },
      stubs: ['Datepicker'],
      localVue,
    })
    const instance = wrapper.findComponent(TaskModal).vm as TaskModalComponent
    instance.showTask({ task: taskData, cb: jest.fn() })
    await wrapper.vm.$nextTick()

    expect(wrapper.find('.modal__header').text()).toBe('Редактирование задачи')
  })

  it('should render header for completed task', async () => {
    taskData.closed = +defDate.add(-1, 'd')
    taskData.isComplete = true
    const wrapper = mount(TaskModal, {
      propsData: {
        task: taskData,
      },
      stubs: ['Datepicker'],
      localVue,
    })
    const instance = wrapper.findComponent(TaskModal).vm as TaskModalComponent
    instance.showTask({ task: taskData, cb: jest.fn() })
    await wrapper.vm.$nextTick()

    expect(wrapper.find('.modal__header').text()).toBe('Данные задачи')
  })
})
