import { shallowMount } from '@vue/test-utils'
import Checkbox from '@/components/_base/ui/Checkbox.vue'

describe('ui/Checkbox.vue', () => {
  it('should render label slot', () => {
    const toBeValue = '<div class="task__title">CheckboxLabel</div>'
    const wrapper = shallowMount(Checkbox, {
      slots: {
        label: toBeValue,
      },
    })

    expect(wrapper.find('.task__title').html()).toBe(toBeValue)
  })

  it('should render error slot', () => {
    const toBeValue = '<div class="task__error">CheckboxLabel</div>'
    const wrapper = shallowMount(Checkbox, {
      slots: {
        error: toBeValue,
      },
    })

    expect(wrapper.find('.task__error').html()).toBe(toBeValue)
  })

  it('should emit change event for single value', async () => {
    const wrapper = shallowMount(Checkbox)
    const elemLabel = wrapper.find('label')
    await elemLabel.trigger('click')
    const elemInput = wrapper.find('input')
    await elemInput.setChecked(false)

    expect(wrapper.emitted().change).toEqual([[true], [false]])
  })

  it('should emit change event for array value', async () => {
    const vmodel = ['a', 'b', 'c']
    const wrapper = shallowMount(Checkbox, {
      model: {
        prop: 'modelValue',
        event: 'change',
      },
      propsData: {
        modelValue: vmodel,
        value: 'a',
      },
    })
    const elemInput = wrapper.find('input')
    await elemInput.setChecked(false)
    await elemInput.setChecked(true)

    expect(wrapper.emitted().change).toEqual([[['b', 'c']], [['a', 'b', 'c']]])
  })
})
