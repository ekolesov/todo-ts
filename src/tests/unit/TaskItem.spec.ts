import { createLocalVue, mount } from '@vue/test-utils'
import TaskItem from '@/components/elems/TaskItem.vue'

import Button from '@/components/_base/ui/Button.vue'
import Checkbox from '@/components/_base/ui/Checkbox.vue'

import { TaskModel } from '@/store/types'
import VueDayJs from '@/core/plugins/dayjs/dayjs-vue'

const localVue = createLocalVue()
localVue.use(VueDayJs)
localVue.component('Button', Button)
localVue.component('Checkbox', Checkbox)

const defDate = localVue.dayjs().startOf('d')

describe('ui/TaskItem.vue', () => {
  let taskData: TaskModel
  beforeEach(() => {
    taskData = {
      id: 'idTask',
      title: 'TaskTitle',
      isComplete: false,
      end: +defDate.add(-1, 'd'),
      closed: null,
    }
  })

  it('should render active task', () => {
    const wrapper = mount(TaskItem, {
      propsData: {
        task: taskData,
      },
      localVue,
    })

    expect(wrapper.element).toMatchSnapshot('activeTaskItem')
  })

  it('should render completed task', () => {
    taskData.closed = +defDate.add(-1, 'd')
    taskData.isComplete = true
    const wrapper = mount(TaskItem, {
      propsData: {
        task: taskData,
      },
      localVue,
    })

    expect(wrapper.element).toMatchSnapshot('completedTaskItem')
  })

  it('should emit save event', async () => {
    const wrapper = mount(TaskItem, {
      propsData: {
        task: taskData,
      },
      localVue,
    })
    const elemCheckbox = wrapper.findComponent(Checkbox).find('label')
    await elemCheckbox.trigger('click')
    const data = wrapper.emitted().save as TaskModel[][]

    expect(data[0][0].title).toBe('TaskTitle')
  })

  it('should emit edit event', async () => {
    const wrapper = mount(TaskItem, {
      propsData: {
        task: taskData,
      },
      localVue,
    })
    const elemCheckbox = wrapper.findComponent({ ref: 'editButton' })
    await elemCheckbox.trigger('click')
    const data = wrapper.emitted().edit as TaskModel[][]

    expect(data[0][0].title).toBe('TaskTitle')
  })

  it('should emit remove event', async () => {
    const wrapper = mount(TaskItem, {
      propsData: {
        task: taskData,
      },
      localVue,
    })
    const elemCheckbox = wrapper.findComponent({ ref: 'removeButton' })
    await elemCheckbox.trigger('click')
    const data = wrapper.emitted().remove as TaskModel[][]

    expect(data[0][0].title).toBe('TaskTitle')
  })
})
