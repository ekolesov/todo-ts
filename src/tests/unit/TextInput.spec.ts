import { shallowMount } from '@vue/test-utils'
import TextInput from '@/components/_base/ui/TextInput.vue'

describe('ui/TextInput.vue', () => {
  it('should render label slot', () => {
    const toBeValue = 'TextInputLabel'
    const wrapper = shallowMount(TextInput, {
      slots: {
        label: toBeValue,
      },
    })

    expect(wrapper.text()).toBe(toBeValue)
  })

  it('should render error slot', () => {
    const toBeValue = 'TextInputError'
    const wrapper = shallowMount(TextInput, {
      slots: {
        error: toBeValue,
      },
    })

    expect(wrapper.text()).toBe(toBeValue)
  })

  it('should render input with placeholder', () => {
    const toBeValue = 'TextInputPlaceholder'
    const wrapper = shallowMount(TextInput, {
      propsData: {
        placeholder: toBeValue,
      },
    })
    const elem = wrapper.find('input')

    expect(elem.attributes().placeholder).toBe(toBeValue)
  })

  it('should render textarea for multiline attribute', () => {
    const wrapper = shallowMount(TextInput, {
      propsData: {
        rows: 5,
      },
      attrs: {
        multiline: '',
      },
    })
    const elem = wrapper.find('textarea')

    expect(elem.attributes().rows).toBe('5')
  })

  it('should render password input', () => {
    const wrapper = shallowMount(TextInput, {
      attrs: {
        password: '',
      },
    })
    const elem = wrapper.find('input')

    expect(elem.attributes().type).toBe('password')
  })

  it('should have focus', () => {
    const div = document.createElement('div')
    div.id = 'rootWrap'
    document.body.appendChild(div)
    const wrapper = shallowMount(TextInput, {
      attachTo: '#rootWrap',
      attrs: {
        focused: '',
      },
    })

    expect(document.activeElement instanceof HTMLInputElement).toBeTruthy()
    wrapper.destroy()
  })

  it('should emit input event', async () => {
    const toBeValue = 'inputValue'
    const wrapper = shallowMount(TextInput)
    const elem = wrapper.find('input')
    await elem.setValue(toBeValue)

    expect(wrapper.emitted().input).toEqual([[toBeValue]])
  })

  it('should emit submit for keyup.enter event', async () => {
    const wrapper = shallowMount(TextInput)
    const elem = wrapper.find('input')
    await elem.trigger('keyup.enter')

    expect(wrapper.emitted().submit).toBeDefined()
  })
})
