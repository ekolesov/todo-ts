import { mount, createLocalVue } from '@vue/test-utils'
import TaskState from '@/components/elems/TaskState.vue'

import VueDayJs from '@/core/plugins/dayjs/dayjs-vue'

import { TaskModel } from '@/store/types'

const localVue = createLocalVue()
localVue.use(VueDayJs)

describe('elems/TaskState.vue', () => {
  let task: TaskModel
  beforeEach(() => {
    task = {
      id: '',
      title: 'taskName',
      isComplete: false,
      end: null,
      closed: null,
    }
  })

  it('should render string state for completed task', () => {
    const toBeValue = 'завершена'
    task.isComplete = true
    const wrapper = mount(TaskState, {
      propsData: { task },
    })

    expect(wrapper.text()).toBe(toBeValue)
  })

  it('should render string state for unlimited task', () => {
    const toBeValue = 'без дедлайна'
    const wrapper = mount(TaskState, {
      propsData: { task },
    })

    expect(wrapper.text()).toBe(toBeValue)
  })

  it('should render string state for today task', () => {
    const toBeValue = 'завершается сегодня'
    task.end = localVue.dayjs().startOf('d')
    const wrapper = mount(TaskState, {
      localVue,
      propsData: { task },
    })

    expect(wrapper.text()).toBe(toBeValue)
  })

  it('should render string state for expired task', () => {
    const toBeValue = 'просрочена на 2 дня'
    task.end = localVue.dayjs().startOf('d').add(-2, 'd')
    const wrapper = mount(TaskState, {
      localVue,
      propsData: { task },
    })

    expect(wrapper.text()).toBe(toBeValue)
  })

  it('should render string state for active task (singular)', () => {
    const toBeValue = 'остался 1 день'
    task.end = localVue.dayjs().startOf('d').add(1, 'd')
    const wrapper = mount(TaskState, {
      localVue,
      propsData: { task },
    })

    expect(wrapper.text()).toBe(toBeValue)
  })

  it('should render string state for active task (plural for 2 days)', () => {
    const toBeValue = 'осталось 2 дня'
    task.end = localVue.dayjs().startOf('d').add(2, 'd')
    const wrapper = mount(TaskState, {
      localVue,
      propsData: { task },
    })

    expect(wrapper.text()).toBe(toBeValue)
  })

  it('should render string state for active task (plural for 25 days)', () => {
    const toBeValue = 'осталось 25 дней'
    task.end = localVue.dayjs().startOf('d').add(25, 'd')
    const wrapper = mount(TaskState, {
      localVue,
      propsData: { task },
    })

    expect(wrapper.text()).toBe(toBeValue)
  })
})
