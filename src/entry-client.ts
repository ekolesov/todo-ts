import 'core-js/stable'
import '@/core/plugins/nprogress'

import { createApp } from './main'
const { app, router, store } = createApp()

const storage = process.env.STORAGE

if (window) {
  window.addEventListener('storage', function (event) {
    if (event.key === 'userData') {
      if (!event.newValue) {
        store.dispatch('AuthData/logout').then(() => {
          if (router.currentRoute.name === 'home') {
            router.push({ name: 'login' })
          }
        })
      } else {
        store
          .dispatch('AuthData/login', JSON.parse(event.newValue))
          .then(() => {
            if (router.currentRoute.name === 'login') {
              router.push({ name: 'home' })
            }
          })
      }
    }
    if (event.key?.startsWith('tasks_')) {
      const userData = window[storage].getItem('userData')
      if (userData) {
        const user = JSON.parse(userData)
        if (!event.newValue) {
          store
            .dispatch('TasksData/clearList')
            .then(() => window[storage].removeItem(`tasks_${user.login}`))
        } else {
          store
            .dispatch('TasksData/setList', JSON.parse(event.newValue))
            .then(() =>
              window[storage].setItem(`tasks_${user.login}`, event.newValue)
            )
        }
      }
    }
  })
}

router.beforeEach((to, from, next) => {
  if (app.$np) app.$np.start()
  if (!store.getters.isAuth) {
    if (to.matched.some((route) => route.meta.requiresAuth)) {
      next({ name: 'login' })
    } else {
      next()
    }
  } else {
    if (to.name === 'login') {
      next({ name: 'home' })
    } else {
      next()
    }
  }
})

router.afterEach(() => {
  if (app.$np) app.$np.done()
})

router.onReady(() => {
  if (window) {
    const storedData = window[storage].getItem('userData')
    if (storedData) {
      store.dispatch('AuthData/login', JSON.parse(storedData)).then(() => {
        if (router.currentRoute.name === 'login') {
          router.push({ name: 'home' })
        }
      })
    } else {
      if (
        router.currentRoute.matched.some((route) => route.meta.requiresAuth)
      ) {
        router.push({ name: 'login' })
      }
    }
  }

  app.$mount('#app')
})
