import { RouteConfig } from 'vue-router/types/router'

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    meta: {
      requiresAuth: true,
    },
    component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue'),
  },
  {
    path: '/login',
    name: 'login',
    component: () =>
      import(/* webpackChunkName: "login" */ '@/views/Login.vue'),
  },

  // Errors
  {
    path: '*',
    name: 'notFound',
    component: () =>
      import(/* webpackChunkName: "notFound" */ '@/views/errors/NotFound.vue'),
  },
]

export default routes
